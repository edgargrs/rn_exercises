interface Persona {
  fullName:string;
  age: number;
  address: Address
}

interface Address {
  number: number;
  street: string;
  country: string;
}

export const ObjetosLiterales = () => {

  const persona: Persona = {
    fullName : "Edgar",
    age: 25,
    address: {
      country: "MX",
      number: 267,
      street:"Rafael Maria Villagran"
    }
  }
  return (
    <>
      <h3>Objetos  Literales</h3>
      <code>
        <pre>
          {JSON.stringify(persona, null, 2 )}
        </pre>
      </code>
    </>
  )
}
