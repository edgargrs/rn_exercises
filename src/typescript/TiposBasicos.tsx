import React from 'react'

export const TiposBasicos = () => {

  let nombre: string = "Edgar";
  const edad = 35;
  const estaActivo:boolean = false;
  const poderes: string[] = ['Velocidad', 'Volar', 'Respirar en el agua'];
  return (
    <>
      <h3>{nombre}, {edad}, {estaActivo ? "Activo" : "No activo"}</h3>
      <br/>
      <p>{poderes.join(", ")}</p>
    </>
  )}
