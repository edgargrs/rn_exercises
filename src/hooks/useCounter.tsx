import { useState } from "react";

export const useCounter = (initial: number = 10) => {
  const [value, setValue] = useState<number>(initial);

  const sum = (num: number) => {
    setValue(value + num);
  }

  return {
    value,
    sum,
  }
}
