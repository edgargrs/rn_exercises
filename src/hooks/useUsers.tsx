import { useEffect, useState, useRef } from 'react'
import { reqResApi } from '../api/reqRes'
import { ReqRespListado, User } from '../interfaces/reqRes';

export const useUsers = () => {
  const [users, setUsers] = useState<User[]>([])

  const pagRef = useRef(1);

  useEffect(() => {
    loadUsers();
  }, [])

  const loadUsers = async () => {
    const resp = await reqResApi.get<ReqRespListado>('/users', {
      params: {
        page: pagRef.current
      }
    })
    if(resp.data.data.length > 0){
      setUsers(resp.data.data);
    }else{
      pagRef.current--;
      alert("No data available");
    }
  }

  const nextPage = () => {
    pagRef.current++;
    loadUsers();
  }

  const previousPage = () => {
    if(pagRef.current > 1){
      pagRef.current--;
      loadUsers();
    }
  }


  return {
    users,
    nextPage,
    previousPage
  }
  
}
