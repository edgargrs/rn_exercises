import { useCounter } from '../hooks/useCounter';

export const ContadorConHook = () => {
  
  const { value, sum } = useCounter();

  return (
    <>
      <h3>Count with hook: <small>{value}</small> </h3>

      <button
        className="btn btn-primary"
        onClick={ () => sum(1)}
      >
        +1
      </button>
      &nbsp;
      <button
        className="btn btn-primary"
        onClick={ () => sum(-1)}
      >
        -1
      </button>
    </>
  )
}
