import React, { useEffect, useReducer } from 'react';

interface AuthState{
  validating: boolean;
  token: string | null;
  userName: string;
  name: string;
}

const initialState: AuthState = {
  validating: true,
  token: null,
  userName: '',
  name: '',
}
type LoginPayload = {
  name: string,
  userName: string
}

type AuthAction = 
| { type: 'logout' }
| { type: 'login', payload: LoginPayload }

const authReducer = (state: AuthState, action: AuthAction): AuthState => {
  switch (action.type) {
    case 'logout':
      return {
        validating: false,
        token: null,
        name: '',
        userName: ''
      }
      case 'login':
        const { name, userName } = action.payload;
        return {
          validating: false,
          token: 'abc123',
          name,
          userName
        }
    default: 
      return state;
  }
}

export const Login = () => {

  const [{validating, token, name}, dispatch] = useReducer(authReducer, initialState);

  const login = () => dispatch({type: 'login', payload:{
    userName: "Estefano",
    name: 'Edgar'
  }});

  const logout = () => dispatch({type: 'logout'});

  useEffect(() => {
    setTimeout(() => {
      dispatch({type: 'logout'});
    }, 1500);
  }, [])

  if(validating){
    return(
      <>
      <div className="alert alert-info">
        Validating...
      </div>
      </>
    );
  }

  return (
    <>

      <h3>Login</h3>

      {
        (token)
          ? <div className="alert alert-success"> Success login as {name}</div>
          : <div className="alert alert-danger" onClick={login}>Login fails</div>
      }

      {
        (token)
          ? <button className="btn btn-danger" onClick={logout}>Logout</button>
          : <button className="btn btn-primary" onClick={login}>Login</button>
      }

    </>
  )
}
