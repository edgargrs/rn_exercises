import { User } from '../interfaces/reqRes';
import { useUsers } from '../hooks/useUsers';

export const Users = () => {
  const { users, nextPage, previousPage } = useUsers();

  const renderItem = ({
    id,
    avatar,
    email,
    first_name,
    last_name,
  }: User ) => {
    return (
      <tr key={id.toString()}>
        <td>
          <img 
            src={avatar}
            alt={first_name}
            style={{ width: 50, borderRadius: 100 }}
          />
        </td>
        <td>{first_name} {last_name}</td>
        <td>{email}</td>
      </tr>
    );
  };

  return (
    <>
      <h3>Users</h3>
      <table className="table">
        <thead>
          <tr>
            <th>Avatar</th>
            <th>Name</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>
          { users.map(renderItem) }
        </tbody>
      </table>
      <button onClick={previousPage} className="btn btn-primary">
        Anteriores
      </button>
      &nbsp;
      <button onClick={nextPage} className="btn btn-primary">
        Siguiente
      </button>
    </>
  )
}
