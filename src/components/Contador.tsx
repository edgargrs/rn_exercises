import React, { useState } from 'react'

export const Contador = () => {
  
  const [value, setValue] = useState<number>(10);

  const sum = (num: number) => {
    setValue(value + num);
  }

  return (
    <>
      <h3>Count <small>{value}</small> </h3>

      <button
        className="btn btn-primary"
        onClick={ () => sum(1)}
      >
        +1
      </button>
      &nbsp;
      <button
        className="btn btn-primary"
        onClick={ () => sum(-1)}
      >
        -1
      </button>
    </>
  )
}
